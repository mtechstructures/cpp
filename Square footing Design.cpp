/*A program for designing of Isolated Square Footing*/
#include<iostream>
#include<math.h>
using namespace std;
int main()
{
	double P,L,B,BC,fck,fy,Areq,side,sides,Aprov,q,d1,d2,d3,d4,k,a,b,b1,D1,D2,M1,M2,L1,L2,pt1,pt2,Minpt,asta,astb,dia,space1,space2,g1,g2,NOB1,NOB2,pt3,pt4,astc,astd,space3,space4,g3,g4,NOB3,NOB4;
	/*USER DATA*/
	//P=Axial Load
	//L=Length of Column
	//B=Breadth of Column
	//BC=Bearing Capacity of soil
	//fck=Grade of Concrete
	//fy=Grade of steel
	cout<<"Enter the Following Data"<<endl;
	cout<<"Enter the Axial Load in kN =" ;
	cin>>P;
	cout<<"Enter the One Side of Column in mm =" ;
	cin>>L;
	cout<<"Enter the Second Side of Column in mm =" ;
	cin>>B;
	cout<<"Enter the Bearing Capacity of soil in kN/m2 =" ;
	cin>>BC;
	cout<<"Enter the Grade of Concrete N/mm2 =" ;
	cin>>fck;
	cout<<"Enter the Grade of Steel in N/mm2 =" ;
	cin>>fy;
	cout<<endl<<endl;
	/**********DESIGN OF SQUARE FOOTING***************/
	//Areq=Area Required for Footing
	//side=Side of Footing
	//sides=Side of footing after rounding off
	//Aprov= Area Provided for Footing
	//q=Calculated Bearing Capacity of Soil
	cout<<"***********Design of Square Footing***************"<<endl<<endl;
	Areq=(P/BC);
	cout<<"\t Required Area of Footing in m = "<<Areq<<endl;
	side=sqrt(Areq);
	cout<<"\t The side of footing in m = "<<side<<endl;
	sides=side+0.5;
	cout<<"\t After rounding off side in m = "<<sides<<endl;
    label1:Aprov=(sides*sides);
	cout<<"\t Area provided in m = "<<Aprov<<endl;
	q=(P/Aprov);
	cout<<"\t q Calculated Bearing Capacity of Soil in kN/m2 = "<<q<<endl;
	cout<<endl;
	cout<<"*****(1.0)Check For (q) Bearing Capacity Of Soil******"<<endl<<endl;
	/*Check for BC*/
	if(q>BC)
	{
		cout<<"\t Increase the Aprov"<<endl;
		goto label1;
	}
	else
	{
		cout<<"\t q Calculated Bearing Capacity of Soil is Ok"<<endl;
	}
	cout<<endl;
	/*Check for one way Shear*/
	//k=coefficient 
	//d1=Depth 1
	//d2=Depth 2
	cout<<"********(2.0)Check For One Way Shear********"<<endl<<endl;
	k=((0.3)/(q/1000));
	d1=(0.5*(((sides*1000)-L)/(k+1)));
	cout<<"\t Depth d1 = "<<d1<<endl;
	d2=(0.5*(((sides*1000)-B)/(k+1)));
	cout<<"\t Depth d2 = "<<d2<<endl;
	/*Greater depth will be chosen*/
	if(d1>d2)
	{
	cout<<"\t Depth d1 is Greater = "<<d1<<endl;
}
    else if(d1<d2)
{
	cout<<"\t Depth d2 is Greater = "<<d2<<endl;
}
    else
    {
    cout<<"\t Both Depths are equal = "<<d1<<endl;
	}
	cout<<endl;
/*check for two way shear*/ 
//a=any ratio which must be greater than b for a required depth
//b=any ratio which must be less than a for required depth d1
//b1=any ratio which must be less than a for required depth d2
cout<<"********(3.0)Check for Two Way Shear********"<<endl<<endl;
label:a=((0.16*sqrt(fck))*1000)/(q);
cout<<"\t a = "<<a<<endl;	
if(d1>d2)
{
b=((Aprov*1000000)-((L+d1)*(B+d1)))/((2*d1)*((L+d1)+(B+d1)));
cout<<"\t b = "<<b<<endl;
}
else
{
b1=((Aprov*1000000)-((L+d2)*(B+d2)))/((2*d2)*((L+d2)+(B+d2)));
cout<<"\t b = "<<b1<<endl;
}
/*Calculations for Depth */
//d3= any variable which stores the value of depth d1 after taking trials
//d4=any variable which stores the value of depth d2 after taking trials
if(a<b)
{
cout<<"\t Increase the Depth d1 by 50mm and take trials"<<endl;
cout<<"\t Enter the d1 again = ";
cin>>d1;
d3=d1;
goto label;
}
else if(a<b1)
{
cout<<"\t Increase the Depth d2 by 50mm and take trials"<<endl;
cout<<"\t enter the d2 again = ";
cin>>d2;
d4=d2;
goto label;
}
else
{ cout<<"\t Depth is Ok"<<endl;
}
cout<<endl;
/*Calculations For Overall Depth*/
//D1=Overall Depth for d1
//D2=Overall Depth for d2
cout<<"*******(4.0)Calculations For Overall Depth********"<<endl<<endl;
if(d1>d2)
{
D1=d3+75;
cout<<"\t Overall Depth in mm = "<<D1<<endl;
}
else
{
D2=d4+75;
cout<<"\t Overall Depth in mm = "<<D2<<endl;
}
cout<<endl;
/*Calculations for Bending moment*/
//L1=Projection beyond the column face along Lengrh wise
//L2=Projection beyond the column face along Breadth wise
//M1=Bending Moment M1
//M2=Bending Moment M2
cout<<"*******(5.0)Calculations For Bending Moment*********"<<endl<<endl;
L1=(((sides-(L/1000))/2)*((sides-(L/1000))/2));
M1=(q*L1)/2;
cout<<"\t Bending Moment M1 in kNm/m = "<<M1<<endl;
L2=(((sides-(B/1000))/2)*((sides-(B/1000))/2));
M2=(q*L2)/2;
cout<<"\t Bending Moment M2 in kNm/m = "<<M2<<endl;
/*calculations for M1*/
//pt1=percentage of steel for depth d3 which stores the value of depth d1
//pt2=percentage of steel for depth d4 which stores the value of depth d2
//asta=area of steel for pt1
//astb=area of steel for pt2
//dia=diameter of bar
//space1 = C/C spacing of bars for asta
//space2=C/C spacing of bars for astb
//g1=Length of Bar
//g2=Length of Bar
//NOB1=No of bars for depth d1
//NOB2= No of bars for depth d2
//Minpt= Minimum pt 85% divided by fy
cout<<endl;
cout<<"******(6.0)Calculations for M1********"<<endl<<endl;
if(d3>d4)
{
pt1=((fck/(2*fy))*(1-sqrt(1-((4.6*M1*1000000*1.5)/(fck*1000*d3*d3)))))*100;
asta=((pt1*1000*d3)/100);
cout<<"\t Enter the dia of bar in mm = ";
cin>>dia;
space1=((M_PI)/4*dia*dia*1000)/asta;
g1=(sides*1000)-75-75;
NOB1=((g1/space1)+1);
}
else
{
pt2=((fck/(2*fy))*(1-sqrt(1-((4.6*M1*1000000*1.5)/(fck*1000*d4*d4)))))*100;
astb=((pt2*1000*d4)/100);
cout<<"\t enter the dia of bar in mm = ";
cin>>dia;
space2=((M_PI)/4*dia*dia*1000)/astb;
g2=(sides*1000)-75-75;
NOB2=((g2/space2)+1);
}
Minpt=(0.85/fy);
cout<<endl;
/*Check For percentage of steel*/
cout<<"******(7.0)Check For Percentage of steel******"<<endl<<endl;
if(pt1>Minpt)
{
cout<<"\t Calculated pt1 in % is OK = "<<pt1<<endl;
cout<<"\t AST in mm2 = "<<asta<<endl;
cout<<"\t C/C spacing in mm = "<<space1<<endl;
cout<<"\t No of bars = "<<NOB1<<endl;
}
else if(pt2>Minpt)
{
cout<<"\t Calculated pt2 in % is OK = "<<pt2<<endl;
cout<<"\t AST in mm2 = "<<astb<<endl;
cout<<"\t C/C spacing in mm = "<<space2<<endl;
cout<<"\t No of bars = "<<NOB2<<endl;
}
else 
{
cout<<"\t Minimum pt in % is adopted or adopt 0.12% = "<<Minpt<<endl;
}
cout<<endl;
/*Calculations for M2*/
//pt3=percentage of steel for depth d3 which stores the value of depth d1
//pt4=percentage of steel for depth d4 which stores the value of depth d2
//astc=area of steel for pt3
//astd=area of steel for pt4
//dia=diameter of bar
//space3 = C/C spacing of bars for astc
//space4=C/C spacing of bars for astd
//g3=Length of Bar
//g4=Length of Bar
//NOB3=No of bars for depth d1
//NOB4= No of bars for depth d2
//Minpt= Minimum percentage of steel 85% diviided by fy
cout<<"********(8.0)Calculations for M2********"<<endl<<endl;
if(d3>d4)
{
pt3=((fck/(2*fy))*(1-sqrt(1-((4.6*M2*1000000*1.5)/(fck*1000*d3*d3)))))*100;
astc=((pt3*1000*d3)/100);
cout<<"\t Enter the dia of bar in mm = ";
cin>>dia;
space3=((M_PI)/4*dia*dia*1000)/astc;
g3=(sides*1000)-75-75;
NOB3=((g3/space3)+1);
}
else
{
pt4=((fck/(2*fy))*(1-sqrt(1-((4.6*M2*1000000*1.5)/(fck*1000*d4*d4)))))*100;
astd=((pt4*1000*d4)/100);
cout<<"\t Enter the dia of bar in mm = ";
cin>>dia;
space4=((M_PI)/4*dia*dia*1000)/astd;
g4=(sides*1000)-75-75;
NOB4=((g4/space4)+1);
}
Minpt=(0.85/fy);
cout<<endl;
/*Check for Percentage of Steel*/
cout<<"*******(9.0)Check For Percentage of Steel********"<<endl<<endl;
if(pt3>Minpt)
{
cout<<"\t Calculated pt3 in % is OK = "<<pt3<<endl;
cout<<"\t AST in mm2 = "<<astc<<endl;
cout<<"\t C/C spacing in mm = "<<space3<<endl;
cout<<"\t No of bars = "<<NOB3<<endl;
}
else if(pt4>Minpt)
{
cout<<"\t Calculated pt4 in % is OK = "<<pt4<<endl;
cout<<"\t AST in mm2 = "<<astd<<endl;
cout<<"\t C/C spacing in mm = "<<space4<<endl;
cout<<"\t No of bars = "<<NOB4<<endl;
}
else 
{
cout<<"\t Minimum pt in % is adopted or adopt 0.12% = "<<Minpt<<endl;
}
cout<<"***********************End Of Programme**************************"<<endl;
return 0;
}


